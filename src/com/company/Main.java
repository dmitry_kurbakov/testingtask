package com.company;

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("<Task 2>");
        System.out.println("List sorted by ascending: " + Task2(Comparator.naturalOrder()));
        System.out.println("List sorted by descending: " + Task2(Comparator.reverseOrder()));
        System.out.println("\n<Task 3>");
        int[] nums = new int[]{0, 1, 2, 3, 4, 5, 10, 20, 35};
        for (int i :
                nums) {
            System.out.println(String.format("Factorial of %s is %s", i, Task3(i)));
        }
        return;
    }

    static String Task2(Comparator order) throws Exception {
        TextController controller = new TextController("src\\data\\numbers.txt");
        String str = controller.readFileAsString();
        List<Integer> intList = controller.getNumsList(str);
        controller.sortBy(intList, order);
        return controller.toString();
    }

    static String Task3(int num){
        return MyMath.getFactorial(num);
    }
}
