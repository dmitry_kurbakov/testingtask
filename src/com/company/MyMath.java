package com.company;

import java.math.BigInteger;

public class MyMath {

    static public  String getFactorial(int n){
        return n >= 20 ? getFactorialUpTo20(n) : getFactorialBiggerThan20(n);
    }

    static private String getFactorialUpTo20(int n){
        long fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        return String.valueOf(fact);
    }

    static private String getFactorialBiggerThan20(int n){
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= n; i++)
            result = result.multiply(BigInteger.valueOf(i));
        return String.valueOf(result);
    }
}
