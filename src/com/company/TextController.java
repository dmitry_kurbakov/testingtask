package com.company;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TextController {

    String filename;
    List<Integer> sortedList;


    public TextController(String filename) {
        this.filename = filename;
    }

    public String readFileAsString() throws Exception{
        String data = new String(Files.readAllBytes(Paths.get(this.filename)));
        return data;
    }

    public List<Integer> getNumsList(String str) {
        List<String> stringItems = Arrays.asList(str.split("\\s*,\\s*"));
        List<Integer> items = new ArrayList<Integer>();
        for (String item:
             stringItems) {
            items.add(Integer.parseInt(item));
        }
        return items;
    }

    public void sortBy(List<Integer> items, Comparator order) {
        items.sort(order);
        this.sortedList = items;
    }

    @Override
    public String toString(){
        StringBuilder resStr = new StringBuilder();
        for (Integer item:
             sortedList) {
            resStr.append(item).append(" ");
        }
        return String.valueOf(resStr);
    }


}
